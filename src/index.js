const express = require("express");
const { initDatabase } = require("./db.js");
const { router } = require("./routers.js");

async function main() {
    const server = express();
    const db = await initDatabase();
    server.use(router);
    server.listen(3000, () => console.log("server working"));
}

main();