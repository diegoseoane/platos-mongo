const mongoose = require('mongoose');
const dotenv = require("dotenv");

async function initDatabase() {

    dotenv.config();
    const pass = process.env.DB_PASS;
    const user = process.env.DB_USER;

    const url = `mongodb+srv://${user}:${pass}@cluster0.n3rw5.mongodb.net/Acamica?retryWrites=true&w=majority`;

    const initDB = new Promise((resolve, reject) => {

        mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', (error) => {
            console.error('connection error:', error);
            reject(error);
        });

        db.once('open', function () {
            resolve(db);
        })
    })

    return initDB;
}

module.exports = {
    initDatabase
}
